package com.ckwen.feign.client;

import com.ckwen.feign.client.api.StaffService;
import com.ckwen.feign.client.model.StaffModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

/**
 * @author wen
 */
@RestController
public class ClientHomeController {

    @Autowired
    private StaffService staffService;

    @RequestMapping
    public String home() {
        return "Client Web";
    }

    @RequestMapping("save")
    public String save() {
        StaffModel newStaff = new StaffModel(String.valueOf(System.currentTimeMillis()), new Random().nextInt(100));
        Integer count = staffService.save(newStaff);
        return "添加了一个新员工，现在共有：" + count + "人";
    }

    @RequestMapping("list")
    public List<StaffModel> list() {
        return staffService.findAll();
    }
}
