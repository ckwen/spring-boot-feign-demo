package com.ckwen.feign.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author wen
 */

@EnableFeignClients(basePackages = "com.ckwen.feign")
@SpringBootApplication
public class ClientWebApplication {

    public static void main(String[] args) {

        SpringApplication.run(ClientWebApplication.class, args);

        System.out.println("【Client】启动成功");

    }

}
