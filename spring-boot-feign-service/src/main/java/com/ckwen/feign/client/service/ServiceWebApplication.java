package com.ckwen.feign.client.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wen
 */

@SpringBootApplication
public class ServiceWebApplication {

    public static void main(String[] args) {

        SpringApplication.run(ServiceWebApplication.class, args);

        System.out.println("【Service】启动成功");

    }

}
