package com.ckwen.feign.client.service;

import com.ckwen.feign.client.api.StaffService;
import com.ckwen.feign.client.model.StaffModel;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wen
 */

@RestController
public class StaffServiceImpl implements StaffService {

    private static List<StaffModel> list = new ArrayList<>();

    @Override
    public List<StaffModel> findAll() {
        return list;
    }

    @Override
    public Integer save(@RequestBody StaffModel model) {
        model.setId(list.size());
        list.add(model);
        return list.size();
    }
}
