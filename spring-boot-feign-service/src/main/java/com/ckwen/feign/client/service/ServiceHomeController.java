package com.ckwen.feign.client.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wen
 */
@RestController
public class ServiceHomeController {

    @RequestMapping
    public String home() {
        return "Service Web";
    }
}
