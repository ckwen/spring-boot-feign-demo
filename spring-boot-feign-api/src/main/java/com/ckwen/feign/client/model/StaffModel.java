package com.ckwen.feign.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wen
 * 员工信息
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StaffModel {

    private Integer id;

    private String number;

    private Integer age;

    public StaffModel(String number, Integer age) {
        this.number = number;
        this.age = age;
    }
}
