package com.ckwen.feign.client.api;

import com.ckwen.feign.client.model.StaffModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author wen
 */

//@FeignClient(value = "hello", url = "${restservice.host}", configuration = ErrorDecoder.class)
//@RequestMapping(value = "/v1/users/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)

@FeignClient(value = "staffService", url = "${service.host}")
public interface StaffService {

    /**
     * 返回所有员工
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    List<StaffModel> findAll();

    /**
     * 保存员工
     *
     * @param model
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    Integer save(StaffModel model);
}
